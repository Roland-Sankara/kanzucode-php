<?php

//read and return unique contents
function readFileContents($filename) {
    $file = fopen($filename, 'r');
    if ($file) {
        $contents = fread($file, filesize($filename));
        fclose($file);
        $contents = preg_replace('/\\$.*?=/', '', $contents);
        $lines = preg_split('/(\p{P}|\s)/', $contents, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $lines = array_map(function($item) {
            return trim($item, '“”');
        }, $lines);
        $lines = array_filter($lines, function($value) {
            return !empty($value) && trim($value) !== '';
        });
        $uniqueLines = array_unique($lines);
        $uniqueLines = array_values($uniqueLines);

        return $uniqueLines;
    } else {
        echo"Can't open file";
        return [];
    }
}

$filename = './test-file.txt';
$contents = readFileContents($filename);
print_r($contents);



//read and return array with punctuation marks
function readFileContent($filename) {
    $file = fopen($filename, 'r');
    if ($file) {
        $contents = fread($file, filesize($filename));
        fclose($file);
        $contents = preg_replace('/\\$.*?=/', '', $contents);
        $lines = preg_split('/(\p{P}|\s)/', $contents, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $lines = array_map(function($item) {
            return trim($item, '“”');
        }, $lines);
        $lines = array_filter($lines, function($value) {
            return !empty($value) && trim($value) !== '';
        });
        $uniqueLines = array_unique($lines);
        $uniqueLines = array_values($uniqueLines);

        $punctuations = [];

        foreach($uniqueLines as $letter) {
            if(ctype_punct($letter)){
                $punctuations[] = $letter;
            }
        }
        return $punctuations;
    } else {
        echo"Can't open file";
        return [];
    }
}

$filename = './test-file.txt';
$contents = readFileContent($filename);
print_r($contents);

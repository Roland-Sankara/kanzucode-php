<?php
$paragraph="This is a paragraph and it has to ﬁnd 256781123456,testemail@gmail.com and https://kanzucode.com/";
//With Reggex
function returnEmailR(string $paragraph): string {
    $pattern = '/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/';
    preg_match_all($pattern, $paragraph, $matches);

    $emails = implode(', ', $matches[0]);
    return $emails;
}

function returnPhoneR(string $paragraph): string {
    preg_match_all('/256\d{7,10}/', $paragraph, $matches);

    $phone = implode(', ', $matches[0]);
    return $phone;
}
function returnUrlR(string $paragraph): string {
    preg_match_all('/https?:\/\/[^\"\s<>]+/', $paragraph, $matches);
    //$matches[0];

    $urls = implode(', ', $matches[0]);
    return $urls;
}

//Without Reggex
function returnEmail(string $paragraph): string {
    $listOf = str_split($paragraph);
    $counter = 0;
    $listOf;
    $email=[];

    function scrapeForEmail($listOf,$index,$endAt): string {
        $startingIndex = $index;
        $endingIndex = 0;
        $ending = str_split($endAt);
        $scraped = "";

        //getting last  index
        $c =0;
        while($c < count($listOf)){
            try{
                if ($listOf[$c]==$ending[0] && $listOf[$c+1]==$ending[1] && $listOf[$c+2]==$ending[2] && $listOf[$c+3]==$ending[3]){
                    $endingIndex = $c+4;
                }
            }catch(Exception $e){
                break;
            }
            $c++;
            
        } 
        //using indicies 

        $sliced_array = array_slice($listOf, $startingIndex, $endingIndex-$startingIndex);        
        return implode($sliced_array);   
    }

    while ($counter < count($listOf)) {
        try{
        if($listOf[$counter]==","){
            $email = scrapeForEmail($listOf,$counter+1,"com ");
        }}catch(Exception $e){
            break; 
        }
        $counter++;
    }

    return $email;
}
function returnPhone(string $paragraph): string {
    $listOf = str_split($paragraph);
    $counter = 0;
    $listOf;
    $phoneNum=[];

    while ($counter < count($listOf)) {
        if(ctype_digit($listOf[$counter])){
            $phoneNum[] = $listOf[$counter];
        }
        $counter++;
    }

    $paragraph = implode($phoneNum);
    return "".$paragraph."";
}

function returnUrl(string $paragraph): string {
    $listOf = str_split($paragraph);
    $counter = 0;
    $listOf;
    $url=[];

    function scrapeUntil($listOf,$index,$endAt): string {
        $startingIndex = $index;
        $endingIndex = 0;
        $ending = str_split($endAt);
        $scraped = "";

        //getting last  index
        $c =0;
        while($c < count($listOf)){
            try{
                if ($listOf[$c]==$ending[0] && $listOf[$c+1]== $ending[1] && $listOf[$c+2]== $ending[2] && $listOf[$c+3]== $ending[3]){
                    $endingIndex = $c+4;
                }
            }catch(Exception $e){
                break;
            }
            $c++;
        } 
        //using indicies 

        $sliced_array = array_slice($listOf, $startingIndex, $endingIndex-$startingIndex);        
        return implode($sliced_array);   
    }

    while ($counter < count($listOf)) {
        try{
        if($listOf[$counter]=="h" && $listOf[$counter+1]=="t" && $listOf[$counter+2]=="t" && $listOf[$counter+3]=="p"){
            $url = scrapeUntil($listOf,$counter,"com/");
        }}catch(Exception $e){
            break; 
        }
        $counter++;
    }
    return $url;

}

//Tests
echo returnPhone($paragraph);
echo"\n";
echo returnUrl($paragraph);
echo"\n";
echo returnEmail($paragraph);
echo"\n";
//Reggex tests
echo returnEmailR($paragraph);
echo"\n";
echo returnUrlR($paragraph);
echo"\n";
echo returnPhoneR($paragraph);
echo"\n";
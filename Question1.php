<?php
function isFibonacci($n) {
    if ($n ==   0 || $n ==   1) {
        return true;
    }
    $num1 =   0;
    $num2 =   1;
    $nextNum = $num1 + $num2;
    while ($nextNum <= $n) {
        if ($nextNum == $n) {
            return true;
        }
        $num1 = $num2;
        $num2 = $nextNum;
        $nextNum = $num1 + $num2;
    }
    return false;
}


$array = range(0,100);
$descendingList;
$fibonacciList=[];
//descending order

$counter = 0;
while ($counter < count($array)) {
    try{
    $descendingList[] =  $array[(count($array)-$counter)-1];
    } catch (Exception $e) {
        break;
    }
    $counter++;
}

$counter = 0;
$fibCounter = 0;
while ($counter < count($descendingList)) {

    if(isFibonacci($descendingList[$counter])){
         if ($fibCounter >= 7){
          $fibonacciList[] = $descendingList[$counter];  
         }
        
        $fibCounter++;
    }
    $counter ++;
}

print_r($fibonacciList);
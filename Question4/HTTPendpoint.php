<?php
require_once("ApiClasses.php");

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age:  3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

if ($uri[1] !== 'project' || $uri[1] !== 'milestone') {

    header("HTTP/1.1  404 Not Found");
    exit();

}

$requestMethod = $_SERVER["REQUEST_METHOD"];
$projectId = isset($uri[2]) ? (int) $uri[2] : null;
$milestoneId = isset($uri[2]) ? (int) $uri[2] : null;
$projectController = new Project();
$milestoneController = new Milestone();

switch ($requestMethod) {

        case 'GET':
            if($uri[1]== "project"){
                if(isset($uri[2]) && $uri[2]== "get"){
                    echo json_encode($projectController->getProjectsForEngineer($_SESSION["Id"]));
                }
            }else{
                header("HTTP/1.1  404 Not Found");
                exit();
            }
            break;
    
            case 'POST':
            if($uri[1]== "project"){
                if(isset($uri[2]) && $uri[2]== "create"){
                    $projectController->createProject($_POST["name"], $_SESSION["Id"]);
                }elseif(isset($uri[2]) && $uri[3]== "update"){
                    $projectController->updateProjectStatus($uri[2],$_POST["Status"], $_SESSION["Id"]);
                }elseif(isset($uri[2]) && $uri[3]== "change"){
                    $projectController->changeEngineer($uri[2],$_POST["newEngineerId"]);
                }elseif(isset($uri[2]) && $uri[3]== "mark"){
                    $projectController->markProjectAsCompleted($uri[2]);
                }else{
                    header("HTTP/1.1  404 Not Found");
                    exit();
                }
            }
            elseif($uri[1]== "milestone"){
                if(isset($uri[2]) && $uri[2]== "create"){
                    $milestoneController->createMilestone($_POST["name"], $_POST["projectId"]);
                }elseif(isset($uri[2]) && $uri[3]== "update"){
                    $milestoneController->updateMilestoneStatus($uri[2], $_POST["Status"]);
                }
            }else{
                header("HTTP/1.1  404 Not Found");
                exit();
            }
            break;
        default:
            header("HTTP/1.1  405 Method Not Allowed");
            exit();
}
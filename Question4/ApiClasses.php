<?php
require_once('DatabaseConfig.php');
/*
Class containing functions to insert and get project related data. 
*/
class Project {
    private $db;
    
  
    public function __construct() {
      $this->db = new dbconnect();
    }
  
    public function createProject($name, $engineerId) {

      $name = filter_var($name, FILTER_SANITIZE_STRING);
      $engineerId = filter_var($engineerId, FILTER_SANITIZE_NUMBER_INT);

      // Logic to create a project
      $results = $this->db->addData("INSERT INTO Project (Name,EngineerId) VALUES ($name,$engineerId);");
          
      if ($results) {
        echo"Project created.";
        return true;
      }else{
        echo"Project not created.";
        return false;
        
      }
    }
  

    public function updateProjectStatus($projectId, $status, $sessionId) {

      //FILTER INPUT
      $projectId = filter_var($projectId, FILTER_SANITIZE_NUMBER_INT);
      $status = filter_var($status, FILTER_SANITIZE_STRING);
      $sessionId = filter_var($sessionId, FILTER_SANITIZE_NUMBER_INT);

      //Check if the current user is an enginneer
      $sessionIdState = true;
      try{
      $result = $this->db->getData("SELECT * FROM Engineer WHERE EngineerId = $sessionId");

      foreach($result as $row){
        $sessionIds = $row["EngineerId"];
        if($sessionId == $sessionIds){
          $sessionIdState = false;
          break;
      }
      }}catch(Exception $e){
        $sessionIdState = true;
      }

      if($sessionIdState){
      $statusState = "";
      try{
      $stateResults = $this->db->getData("SELECT status FROM Project WHERE ProjectId = $projectId.");

      foreach($stateResults as $row){
        $statusState = $row["Status"];
      }
      }catch(Exception $e){
        goto Here;

      }
      //check if the state is Completed
      if ($statusState != "Completed") {
      $results = $this->db->addData("UPDATE Project SET Status = $status WHERE ProjectId = $projectId;");
      }else{
        echo"Project has been completed.";  
        return false;
      }
          
      if ($results) {
        echo"Project updated.";
        return true;
      }else{
        Here:
        echo"Project not updated.";
        return false;
      }
    }else{
      echo "User not permitted";
    }

    }
  
    public function getProjectsForEngineer($engineerId) {

      $engineerId = filter_var($engineerId, FILTER_SANITIZE_NUMBER_INT);
      // Logic to get projects for an engineer

      try{
          $results = $this->db->getData("SELECT * FROM Project WHERE EngineerId = $engineerId.");           
      }catch(Exception $e){
        return null;
      }
      return $results;
    }
  
    public function changeEngineer($projectId, $newEngineerId) {

      $newEngineerId = filter_var($newEngineerId, FILTER_SANITIZE_NUMBER_INT);
      $projectId = filter_var($projectId, FILTER_SANITIZE_NUMBER_INT);

      // Logic to change the engineer assigned to a project

      $results = $this->db->addData("UPDATE Project SET EngineerId = $newEngineerId WHERE ProjectId = $projectId ;");
      
      if ($results) {
        echo"Project updated.";
        return true;
      }else{
        echo"Project not updated.";
        return false;
      }
    }

    public function markProjectAsCompleted($projectId){

      $projectId = filter_var($projectId, FILTER_SANITIZE_NUMBER_INT);
      // Logic to mark a project as completed

      $results = $this->db->addData("UPDATE Project SET Status = Completed WHERE ProjectId = $projectId ;");
      
      if ($results) {
        echo"Project updated.";
        return true;
      }else{
        echo"Project not updated.";
        return false;
      }
    }
  }
  

  /*
  Class containing functions to manipulate project milestones.
  */
  class Milestone {

    private $db;
  
    public function __construct() {
      $this->db = new dbconnect();
    }
  
    public function createMilestone($projectId, $name) {

      $projectId = filter_var($projectId, FILTER_SANITIZE_NUMBER_INT);
      $name = filter_var($name, FILTER_SANITIZE_STRING);

      // Logic to create a milestone
      
      $results = $this->db->addData("INSERT INTO Milestone (Name,ProjectId) VALUES ($name,$projectId);");
          
      if ($results) {
        echo"Milestone created.";
        return true;
      }else{
        echo"Milestone not created.";
        return false;
      }

    }
  
    public function updateMilestoneStatus($milestoneId, $status) {

      $milestoneId = filter_var($milestoneId, FILTER_SANITIZE_NUMBER_INT);
      $status = filter_var($status, FILTER_SANITIZE_STRING);

      $results = $this->db->addData("UPDATE Milestone SET Status = $status WHERE MilestoneId = $milestoneId ;");

      if ($results) {
        echo"Milestone updated.";
        return true;
      }else{
        echo"Milestone not updated.";
        return false;
      }
    }
    
}
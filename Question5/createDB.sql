SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Jaba
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Jaba
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Jaba` ;
USE `Jaba` ;

-- -----------------------------------------------------
-- Table `Jaba`.`NGO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`NGO` (
  `NGOId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL,
  `Status` VARCHAR(45) NULL,
  PRIMARY KEY (`NGOId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`Form`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`Form` (
  `FormId` INT NOT NULL AUTO_INCREMENT,
  `FormName` VARCHAR(45) NULL,
  `FormDescription` VARCHAR(75) NULL,
  `NGOId` INT NULL,
  PRIMARY KEY (`FormId`),
  INDEX `fk_Form_1_idx` (`NGOId` ASC) VISIBLE,
  CONSTRAINT `fk_Form_1`
    FOREIGN KEY (`NGOId`)
    REFERENCES `Jaba`.`NGO` (`NGOId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`Field`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`Field` (
  `FieldId` INT NOT NULL AUTO_INCREMENT,
  `FormId` INT NULL,
  `FieldName` VARCHAR(45) NULL,
  `Type` VARCHAR(45) NULL,
  PRIMARY KEY (`FieldId`),
  INDEX `fk_Field_1_idx` (`FormId` ASC) VISIBLE,
  CONSTRAINT `fk_Field_1`
    FOREIGN KEY (`FormId`)
    REFERENCES `Jaba`.`Form` (`FormId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`FormEntry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`FormEntry` (
  `FormEntryId` INT NOT NULL AUTO_INCREMENT,
  `FormId` INT NULL,
  `FormEntryData` VARCHAR(255) NULL,
  PRIMARY KEY (`FormEntryId`),
  CONSTRAINT `fk_FormEntry_1`
    FOREIGN KEY (`FormEntryId`)
    REFERENCES `Jaba`.`Form` (`FormId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`FieldData`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`FieldData` (
  `FieldDataId` INT NOT NULL AUTO_INCREMENT,
  `FormEntryId` INT NULL,
  `FieldId` INT NULL,
  `Data` VARCHAR(45) NULL,
  PRIMARY KEY (`FieldDataId`),
  INDEX `fk_FieldData_1_idx` (`FormEntryId` ASC) VISIBLE,
  INDEX `fk_FieldData_2_idx` (`FieldId` ASC) VISIBLE,
  CONSTRAINT `fk_FieldData_1`
    FOREIGN KEY (`FormEntryId`)
    REFERENCES `Jaba`.`FormEntry` (`FormEntryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FieldData_2`
    FOREIGN KEY (`FieldId`)
    REFERENCES `Jaba`.`Field` (`FieldId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`Staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`Staff` (
  `StaffId` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NULL,
  `OtherName(s)` VARCHAR(45) NULL,
  `Contact` VARCHAR(45) NULL,
  `DOB` DATE NULL,
  PRIMARY KEY (`StaffId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`FormEntryStaff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`FormEntryStaff` (
  `FormEntryId` INT NOT NULL AUTO_INCREMENT,
  `StaffId` INT NULL,
  `EntryDate` DATETIME NULL,
  PRIMARY KEY (`FormEntryId`),
  INDEX `fk_FormEntryStaff_1_idx` (`StaffId` ASC) VISIBLE,
  CONSTRAINT `fk_FormEntryStaff_1`
    FOREIGN KEY (`StaffId`)
    REFERENCES `Jaba`.`Staff` (`StaffId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormEntryStaff_2`
    FOREIGN KEY (`FormEntryId`)
    REFERENCES `Jaba`.`FormEntry` (`FormEntryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Jaba`.`FormAudit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`FormAudit` (
  `AuditId` INT NULL DEFAULT NULL AUTO_INCREMENT,
  `FormId` INT NOT NULL,
  `ActionType` ENUM('INSERT', 'UPDATE', 'DELETE') NOT NULL,
  `OldValue` VARCHAR(255) NULL DEFAULT NULL,
  `NewValue` VARCHAR(255) NULL DEFAULT NULL,
  `AuditDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AuditBy` INT NULL,
  PRIMARY KEY (`AuditId`),
  INDEX `KEY` (`FormId` ASC) VISIBLE,
  INDEX `fk_FormAudit_1_idx` (`AuditBy` ASC) VISIBLE,
  CONSTRAINT `KEY`
    FOREIGN KEY (`FormId`)
    REFERENCES `Jaba`.`Form` (`FormId`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_FormAudit_1`
    FOREIGN KEY (`AuditBy`)
    REFERENCES `Jaba`.`Staff` (`StaffId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `Jaba`.`FormEntryAudit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`FormEntryAudit` (
  `AuditID` INT NULL DEFAULT NULL AUTO_INCREMENT,
  `EntryId` INT NOT NULL,
  `ActionType` ENUM('INSERT', 'UPDATE', 'DELETE') NOT NULL,
  `OldValue` VARCHAR(255) NULL DEFAULT NULL,
  `NewValue` VARCHAR(255) NULL DEFAULT NULL,
  `AuditDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditID`),
  INDEX (`EntryId` ASC) VISIBLE,
  CONSTRAINT ``
    FOREIGN KEY (`EntryId`)
    REFERENCES `Jaba`.`FormEntry` (`FormEntryId`));


-- -----------------------------------------------------
-- Table `Jaba`.`StaffAudit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Jaba`.`StaffAudit` (
  `AuditId` INT NULL DEFAULT NULL AUTO_INCREMENT,
  `StaffId` INT NOT NULL,
  `ActionType` VARCHAR(50) NOT NULL,
  `OldValue` VARCHAR(255) NULL DEFAULT NULL,
  `NewValue` VARCHAR(255) NULL DEFAULT NULL,
  `AuditDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditId`),
  CONSTRAINT ``
    FOREIGN KEY ()
    REFERENCES `Jaba`.`Staff` ()
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;